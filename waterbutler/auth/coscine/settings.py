from waterbutler import settings

config = settings.child('COSCINE_AUTH_CONFIG')


JWT_EXPIRATION = int(config.get('JWT_EXPIRATION', 15))
JWT_ALGORITHM = config.get('JWT_ALGORITHM', 'HS256')
API_URL = config.get('API_URL', 'http://localhost:5000/api/v1/files/auth/')

JWT_SECRET = config.get('JWT_SECRET')
JWT_AUDIENCE = config.get('JWT_AUDIENCE')
JWT_ISSUER = config.get('JWT_ISSUER')

JWE_SALT = config.get('JWE_SALT')
JWE_SECRET = config.get('JWE_SECRET')

assert JWT_SECRET, 'JWT_SECRET must be specified. See coscine/global/jwtsecret'
assert JWT_AUDIENCE, 'JWT_AUDIENCE must be specified. See coscine/global/jwtaudience'
assert JWT_ISSUER, 'JWE_SALT must be specified. See coscine/global/jwtissuer'

assert JWE_SALT, 'JWE_SALT must be specified. See waterbutler/jwesalt'
assert JWE_SECRET, 'JWE_SECRET must be specified. See waterbutler/jwesecret'

MFR_ACTION_HEADER = config.get('MFR_ACTION_HEADER', 'X-Cos-Mfr-Request-Action')
